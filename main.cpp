#include <iostream>

#include "utils/Options.h"
#include "utils/types.h"
#include "utils/generate.h"



int main(i32 argc, c8 **argv) {
  if (argc < 6) {
    std::cout << "[USAGE] ./generate_matrix <type> <N> <nnz_per_row> <cache line size> <variance> <out file>";
    exit(-1);
  }

  Options options = Options::parse(argc, argv);
  generate_matrix(options);

  return 0;
}
