//
// Created by lnghrdntcr on 11/14/22.
//

#pragma once

#include <cmath>
#include <vector>
#include <set>
#include <random>
#include <functional>

#include "types.h"
#include "utils.h"
#include "Options.h"

#define GEN_MATRIX_PROLOGUE                               \
  const auto N = options.N();                             \
  const u32 nnz_per_row = options.nnz_per_row();          \
  u32 nnz = N * nnz_per_row;                              \
  const auto cache_line_size = options.cache_line_size(); \
  const u32 stride = (f32) cache_line_size / 4.0f;        \
  auto *indptr = new u32[N + 1];                          \
  auto *indices = new u32[nnz];                           \
  auto int_rng = get_uniform_uint_rng(0, N);              \
  auto float_rng = get_uniform_real_rng(0.0f, 1.0f);      \
  indptr[0] = 0;                                          \

#define GEN_MATRIX_EPILOGUE                                                   \
  do {                                                                        \
    auto *v = new f32[added_nnz];                                             \
    std::generate(v, v + added_nnz, float_rng);                               \
    u32 *coo_x = nullptr;                                                     \
    u32 *coo_y = nullptr;                                                     \
    csr_to_coo(indices, indptr, &coo_x, &coo_y, N, added_nnz);                \
    matrix_write(coo_x, coo_y, v, N, added_nnz, options.out_file(), options); \
    std::free(coo_x);                                                         \
    std::free(coo_y);                                                         \
    std::free(indptr);                                                        \
    std::free(indices);                                                       \
    std::free(v);                                                             \
    return 0;                                                                 \
                                                                              \
  } while(0)


std::function<f32(void)> get_uniform_real_rng(f32 lbound = 0.0f, f32 ubound = 1.0f) {
  std::random_device dev{};
  std::mt19937_64 engine{dev()};

  std::uniform_real_distribution<f32> uniform_distribution(lbound, ubound);
  return [=]() mutable -> f32 {
    return uniform_distribution(engine);
  };
}

std::function<u32(void)> get_uniform_uint_rng(const u32 lbound, const u32 ubound) {
  std::random_device dev{};
  std::mt19937 engine{dev()};

  // Uniform int distribution yields number in the range [lbound, ubound]
  // But I want [lbound, ubound)
  std::uniform_int_distribution<u32> uniform_distribution(lbound, ubound - 1);
  return [=]() mutable -> u32 {
    return uniform_distribution(engine);
  };
}

template<typename icdf_t>
u32 generate_with_icdf_csr(const u32 N, const u32 nnz, const u32 stride, u32 *indices, u32 *indptr,
                           icdf_t icdf) {
  u32 added_nnz = 0;

  bool flag_break = false;
  auto int_rng = get_uniform_uint_rng(0, N);
  auto float_rng = get_uniform_real_rng(0.0f, 1.0f);
  indptr[0] = 0;
  for (u32 i = 0; i < N; ++i) {
    const f32 sample = float_rng();
    u32 nnz_per_row = icdf(sample);

    for (u32 j = 0; j < nnz_per_row; ++j, ++added_nnz) {

      if (i * nnz_per_row + j >= nnz) {
        flag_break = true;
        nnz_per_row = j;
        break;
      }

      indices[added_nnz] = int_rng();
    }

    indptr[i + 1] = indptr[i] + nnz_per_row;
    if (flag_break) break;

  }

  return added_nnz;
}

template<typename T>
T max(T *v, u32 length) {

  typename std::remove_const<T>::type c_max = 0;

  for (u32 i = 0; i < length; ++i)
    if (v[i] > c_max) c_max = v[i];


  return c_max;
}

void matrix_write(u32 *x, u32 *y, const f32 *v, const u32 N, const u32 nnz, const std::string &out_path,
                  const Options &options) {
  std::fstream matrix_out_file(out_path, std::ios::out | std::ios::app);

  if (!matrix_out_file.is_open()) {
    throw std::runtime_error("No such file or directory");
  }

  // Just for representation purposes
  // std::swap(x, y);

  // write header
  matrix_out_file << "%%MatrixMarket matrix coordinate real general\n";
  matrix_out_file << "%-------------------------------------------------------------------------------\n";
  matrix_out_file << "% Barcelona Supercomputing Center (BSC-CNS)\n";
  matrix_out_file << "% author: Francesco Sgherzi\n";
  matrix_out_file << "% kind: " << options.matrix_type_str() << "\n";
  matrix_out_file << "%-------------------------------------------------------------------------------\n";

  u32 actual_N = std::max(max(x, nnz), max(y, nnz)) + 1;

  matrix_out_file << std::to_string(actual_N) << " " << std::to_string(actual_N) << " " << std::to_string(nnz) << "\n";

  for (u32 i = 0; i < nnz; ++i)
    matrix_out_file << std::to_string(x[i] + 1) << " " << std::to_string(y[i] + 1) << " " << std::to_string(v[i])
                    << "\n";

  matrix_out_file.close();
}

void csr_to_coo(const u32 *indices, const u32 *indptr, u32 **x, u32 **y, u32 N, u32 nnz) {

  *x = new u32[nnz];
  *y = new u32[nnz];

  for (u32 i = 0; i < N; ++i) {
    auto begin = indptr[i];
    auto end = indptr[i + 1];

    for (u32 j = begin; j < end; ++j) {
      (*x)[j] = indices[j];
      (*y)[j] = i;
    }
  }
}

// COO Generated matrices
i32 generate_random_matrix(const Options &options) {
  // Generate using COO
  const auto N = options.N();
  const u32 nnz = N * options.nnz_per_row();

  auto *x = new u32[nnz];
  auto *y = new u32[nnz];
  auto *v = new f32[nnz];

  auto int_rng = get_uniform_uint_rng(0, N);
  auto float_rng = get_uniform_real_rng(0.0f, 1.0f);

  // y is contiguous, x is random
  std::generate(x, x + nnz, [&int_rng]() { return int_rng(); });
  std::generate(y, y + nnz, [&int_rng]() { return int_rng(); });
  std::generate(v, v + nnz, [&float_rng]() { return float_rng(); });

  std::sort(y, y + nnz);

  matrix_write(x, y, v, N, nnz, options.out_file(), options);
  return 0;
}

i32 generate_ocol_matrix(const Options &options) {
  const auto N = options.N();
  const u32 nnz = N;

  auto *x = new u32[nnz];
  auto *y = new u32[nnz];
  auto *v = new f32[nnz];

  auto float_rng = get_uniform_real_rng(0.0f, 1.0f);
  std::generate(v, v + nnz, [float_rng]() { return float_rng(); });
  auto y_pos = get_uniform_uint_rng(0, N)();
  for (u32 i = 0; i < nnz; ++i) {
    x[i] = i;
    y[i] = y_pos;
  }

  matrix_write(x, y, v, N, nnz, options.out_file(), options);
  return 0;

}

i32 generate_orow_matrix(const Options &options) {
  const auto N = options.N();
  const u32 nnz = N;

  auto *x = new u32[nnz];
  auto *y = new u32[nnz];
  auto *v = new f32[nnz];

  auto float_rng = get_uniform_real_rng(0.0f, 1.0f);
  auto x_pos = get_uniform_uint_rng(0, N)();

  std::generate(v, v + nnz, [float_rng]() { return float_rng(); });

  for (u32 i = 0; i < nnz; ++i) {
    x[i] = x_pos;
    y[i] = i;
  }

  matrix_write(x, y, v, N, nnz, options.out_file(), options);
  return 0;

}

i32 generate_stride_matrix(const Options &options) {

  GEN_MATRIX_PROLOGUE;

  u32 pos = int_rng();

  for (u32 i = 0; i < N; ++i) {
    for (u32 j = 0; j < nnz_per_row; ++j) {
      indices[i * nnz_per_row + j] = (pos += stride) % N;
    }
    indptr[i + 1] = indptr[i] + nnz_per_row;
  }

  u32 added_nnz = nnz;

  GEN_MATRIX_EPILOGUE;

}


i32 generate_gaussian_matrix(const Options &options) {

  GEN_MATRIX_PROLOGUE;

  nnz *= 1.5f;
  const auto mean = (f32) nnz / N;
  const auto variance = mean * options.variance();
  const auto stddev = std::sqrt(variance);

  auto inv_gaussian_cdf = [&mean, &stddev](const f32 _x) {
    return mean + std::sqrt(2) * stddev * erfinv(2 * _x - 1);
  };

  auto added_nnz = generate_with_icdf_csr(N, nnz, 0, indices, indptr, inv_gaussian_cdf);
  GEN_MATRIX_EPILOGUE;

}

i32 generate_exp_matrix(const Options &options) {
  GEN_MATRIX_PROLOGUE;

  const auto inv_exp = [&nnz_per_row](const f32 _x) {
    return -1.0f * std::log(1 - _x);
  };

  auto added_nnz = generate_with_icdf_csr(N, nnz, 0, indices, indptr, inv_exp);

  GEN_MATRIX_EPILOGUE;
}

i32 generate_uniform_matrix(const Options &options) {

  GEN_MATRIX_PROLOGUE;
  nnz *= 1.5f;

  const auto b = (f32) nnz / N; //std::sqrt(3) * stddev + mean;
  const auto a = (f32) 0; //mean - std::sqrt(3) * stddev;

  const auto inv_uniform = [&a, &b](const f32 _x) {
    return a + _x * b;
  };

  auto added_nnz = generate_with_icdf_csr(N, nnz, 0, indices, indptr, inv_uniform);

  GEN_MATRIX_EPILOGUE;
}

i32 generate_cyclic_matrix(const Options &options) {

  GEN_MATRIX_PROLOGUE;
  const auto pattern_length = options.cache_line_size();
  nnz *= 2.0f;
  auto pattern_rng = get_uniform_uint_rng(0, options.nnz_per_row() * 2);
  bool flag_break = false;
  u32 added_nnz = 0;
  std::vector<u32> nnzs_per_pattern_item(pattern_length, 0);
  for (auto &v: nnzs_per_pattern_item)
    v = pattern_rng();

  for (u32 i = 0; i < N; ++i) {
    u32 nnz_per_row = nnzs_per_pattern_item[i % pattern_length];
    for (u32 j = 0; j < nnz_per_row; ++j, ++added_nnz) {
      if (i * nnz_per_row + j >= nnz) {
        flag_break = true;
        nnz_per_row = j;
        break;
      }
      indices[added_nnz] = int_rng();
    }
    indptr[i + 1] = indptr[i] + nnz_per_row;
    if (flag_break) break;
  }

  GEN_MATRIX_EPILOGUE;


}

i32 generate_spatial_locality_matrix(const Options &options) {
  GEN_MATRIX_PROLOGUE;
  auto initial_position_rng = get_uniform_uint_rng(0, N - nnz_per_row);
  u32 added_nnz = 0;

  for (u32 i = 0; i < N; ++i) {
    const auto pos_x = initial_position_rng();
    for (u32 j = 0; j < nnz_per_row; ++j, ++added_nnz) {
      indices[added_nnz] = pos_x + j;
    }
    indptr[i + 1] = indptr[i] + nnz_per_row;
  }

  GEN_MATRIX_EPILOGUE;

}


i32 generate_temporal_locality_matrix(const Options &options) {
  GEN_MATRIX_PROLOGUE;
  u32 added_nnz = 0;

  std::vector<u32> row_idxs(nnz_per_row);
  std::generate(row_idxs.begin(), row_idxs.end(), int_rng);

  for (u32 i = 0; i < N; ++i) {
    for (u32 j = 0; j < nnz_per_row; ++j, ++added_nnz) {
      indices[added_nnz] = row_idxs[j];
    }
    indptr[i + 1] = indptr[i] + nnz_per_row;
  }

  GEN_MATRIX_EPILOGUE;
}

i32 generate_matrix(const Options &options) {

  if (options.matrix_type() == MatrixType::RANDOM) return generate_random_matrix(options);
  if (options.matrix_type() == MatrixType::STRIDE) return generate_stride_matrix(options);
  if (options.matrix_type() == MatrixType::ONLY_COL) return generate_ocol_matrix(options);
  if (options.matrix_type() == MatrixType::ONLY_ROW) return generate_orow_matrix(options);
  if (options.matrix_type() == MatrixType::LOCALITY_SP) return generate_spatial_locality_matrix(options);
  if (options.matrix_type() == MatrixType::LOCALITY_TP) return generate_temporal_locality_matrix(options);


  if (options.matrix_type() == MatrixType::GAUSSIAN) return generate_gaussian_matrix(options);
  if (options.matrix_type() == MatrixType::UNIFORM) return generate_uniform_matrix(options);
  if (options.matrix_type() == MatrixType::EXP) return generate_exp_matrix(options);
  if (options.matrix_type() == MatrixType::CYCLIC) return generate_cyclic_matrix(options);


  ASSERT_NOT_REACHED();
}


