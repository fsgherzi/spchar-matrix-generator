//
// Created by lnghrdntcr on 11/14/22.
//
#pragma once

#include "types.h"

#include "utils.h"

using namespace fs;

class Options {

public:
  static Options parse(i32 argc, c8 **argv) {
    u32 args = 1;

    auto matrix_type_str = std::string(argv[args++]);
    auto mt = matrix_type_from_string(matrix_type_str);
    auto N = std::atoi(argv[args++]);
    auto nnz_per_row = std::atoi(argv[args++]);
    auto cl_size = std::atoi(argv[args++]);
    auto variance = std::atof(argv[args++]);
    auto out_file = std::string(argv[args++]);

    return Options(N, nnz_per_row, mt, cl_size, variance, out_file, matrix_type_str);
  }

  const u32 N() const {
    return m_N;
  }

  const u32 nnz_per_row() const {
    return m_nnz_per_row;
  }

  const f32 variance() const {
    return m_variance;
  }

  const MatrixType matrix_type() const {
    return m_type;
  }

  const u32 cache_line_size() const {
    return m_cache_line_size;
  }

  const std::string &out_file() const {
    return m_out_file;
  }

  const std::string &matrix_type_str() const {
    return m_matrix_type_str;
  }

private:
  Options(u32 N, u32 nnz, MatrixType mt, u32 cache_line_size, f32 variance, std::string &out_file,
          std::string &matrix_type_str) :
    m_N(N), m_nnz_per_row(nnz), m_type(mt), m_cache_line_size(cache_line_size), m_variance(variance), m_out_file(out_file),
    m_matrix_type_str(matrix_type_str) {}

  const u32 m_N;
  const u32 m_nnz_per_row;
  const MatrixType m_type;
  const u32 m_cache_line_size;
  const std::string m_out_file;
  const std::string m_matrix_type_str;
  const f32 m_variance;

};


