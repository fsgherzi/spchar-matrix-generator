//
// Created by lnghrdntcr on 11/14/22.
//
#pragma once

#include <algorithm>
#include <string>
#include "fstypes.h"

using namespace fs;

enum MatrixType {
    RANDOM,      // Fully random
    ONLY_ROW,    // First row
    ONLY_COL,    // First column
    STRIDE,      // Strided @CL size
    GAUSSIAN,    // Random - Gaussian
    UNIFORM,     // Random - Uniform
    EXP,         // Random - Exponential
    CYCLIC,      // Cyclic pattern
    LOCALITY_SP, // Spatial locality
    LOCALITY_TP, // Temporal locality
};

enum MatrixFormat {
  COO,
  CSR
};