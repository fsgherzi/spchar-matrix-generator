//
// Created by lnghrdntcr on 8/3/22.
//
#pragma once
#include <chrono>
#include <string>
#include <iostream>

using sys_clock = std::chrono::system_clock;

namespace fs {
    template <typename FunctionType>
    unsigned time_it(FunctionType function, const std::string& name = "", const bool debug = false) {
        auto begin = sys_clock::now();
        function();
        auto end = sys_clock::now();
        auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();
        if (debug) std::cout << name << " took " << elapsed  << "us" << std::endl;
        return elapsed;
    }
}
