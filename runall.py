#!/usr/bin/python

import sys
from scipy.io import mmread
from scipy.sparse import csr_matrix
import matplotlib.pylab as plt
from tqdm import tqdm
import os
import numpy as np
import seaborn as sns

N = 10000
nnz_per_row = 16


def generate_matrices(dest_dir):

    if not os.path.exists(dest_dir):
        os.mkdir(dest_dir)

    os.system(f"rm {dest_dir}/*")
    all_kinds = ["random", "col", "row", "stride", "gaussian", "uniform", "exp", "cyclic", "spatial", "temporal"]
    for kind in tqdm(all_kinds):
        if kind in ["random", "col", "row", "exp", "uniform", "gaussian", "spatial", "temporal"]:
            cl_sizes = [64]
        elif kind == "cyclic":
            cl_sizes = [2**i for i in range(8)]
        else:
            cl_sizes = [64, 128, 256]
        for cl_size in cl_sizes:
            os.system(
                f"./cmake-build-debug/generate_matrix {kind} {N} {nnz_per_row} {cl_size} 0.3 {dest_dir}/{kind}-{N}-{nnz_per_row}-0.1-{cl_size}.mtx")


def generate_plots(base_path, dest_path):

    for path in [base_path, dest_path]:
        if not os.path.exists(path):
            os.mkdir(path)

    os.system(f"rm {dest_path}/*")
    matrices = sorted(os.listdir(base_path))
    matrices_path = [f"{base_path}/{m}" for m in matrices]
    for i, matrix in tqdm(enumerate(matrices_path), desc="matrix", total = len(matrices_path)):
        matrix_name = matrices[i].replace(".mtx", "")
        mcoo = mmread(matrix)

        if "row" in matrix_name or "col" in matrix_name:
            m = csr_matrix(mcoo)
        else:
            m = csr_matrix(np.transpose(mcoo))

        ## Plot matrix
        plt.figure(figsize=(10, 10), dpi=100)
        plt.spy(m, markersize=1, color="black", marker=".")
        plt.title(matrix.replace(base_path + "/", "").replace(".mtx", ""))
        plt.savefig(dest_path + "/" + matrix_name + ".png")
        plt.close()

        ## Do histogram
        num_elems = []
        for j in tqdm(range(m.shape[0]), desc="hist", leave=False):
            num_elems.append(m.indptr[j + 1] - m.indptr[j])

        ## Plot densities
        plt.figure(figsize=(10, 10), dpi=100)
        ax = sns.kdeplot(num_elems, color='red', shade=True, warn_singular=False)
        llim, ulim = ax.get_xlim()
        ax.set_xlim((max(0, llim), ulim))
        plt.title("nnz/row distribution of " + matrices[i])
        plt.savefig(dest_path + "/" + "kde-" + matrix_name + ".png")
        plt.close()

        ## Plot histogram
        plt.figure(figsize=(10, 10), dpi=100)
        sns.histplot(num_elems)
        plt.title("nnz/row distribution of " + matrices[i])
        plt.savefig(dest_path + "/" + "hist-" + matrix_name + ".png")
        plt.close()


def main(argv):
    if argv[1] == "generate":
        generate_matrices(argv[2])

    if argv[1] == "images":
        generate_plots(argv[2], argv[3])

    if argv[1] == "all":
        generate_matrices(argv[2])
        generate_plots(argv[2], argv[3])


if __name__ == "__main__":
    if len(sys.argv) not in [3, 4]:
        print("[USAGE] python runall.py [generate | images | all] matrix_srcdir destdir. \nOption `generate` requires just the matrix_srcdir argument")
        sys.exit(-1)

    main(sys.argv)
