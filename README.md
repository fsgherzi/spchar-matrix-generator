# Matrix Generator

## Build
Compile the generator using CMake: 
```bash
mkdir cmake-build-debug
cd cmake-build-debug/
cmake ..
make
```

Install python dependencies for plotting: 
```bash
pip install -r requirements.txt
```

## Examples
Launch the `runall.py` script. Launch it without arguments to see the options.

Generate the matrices: 
```bash
./runall.py generate mtx_dir
```

Generate plots:
```bash
./runall.py images mtx_dir images_dest_dir
```

Do it all together:
```bash
./runall.py all mtx_dir images_dest_dir
```

## Some generated matrices
| Matrix                                                             | Non zero distribution                                            |
|--------------------------------------------------------------------|------------------------------------------------------------------|
| ![Cyclic pattern](readme_images/cyclic-10000-16-0.1-16.png)        | ![Cyclic pattern](readme_images/hist-cyclic-10000-16-0.1-16.png) |
| ![Exponential Distribution](readme_images/exp-10000-16-0.1-64.png) | ![Density plot](readme_images/hist-exp-10000-16-0.1-64.png)      |
 | ![Stride 64](readme_images/stride-10000-16-0.1-64.png)             | ![Stride 64](readme_images/hist-stride-10000-16-0.1-64.png)      |